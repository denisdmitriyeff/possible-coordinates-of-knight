#Possible coordinates of knight

_Possible coordinates of knight_ is a simple page that displays all possible coordinates of a chess knight from a given position.

**Example:**

input: D4

output: C2 E2 F3 F5 E6 C6 B3 B5

###Installation##

```$ git clone https://bitbucket.org/denisdmitriyeff/possible-coordinates-of-knight.git```

or simply upload all given files to your computer.

###How to start

Open **index.html** file in your browser.