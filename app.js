document.querySelector('.btn').addEventListener('click', function() {

  const results = document.getElementById('result');
  const possiblePositions = [];
  const positionsToShow = [];
  const xCoordinates = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
  const cel = document.getElementById('input').value; // saves input coordinates
  const cell = cel.toUpperCase(); // possible to write in input capital or small letters
  const cellX = xCoordinates.indexOf(cell[0]) + 1; // number of x coordinate on the board
  const cellY = parseInt(cell[1]); // number of y coordinate on the board

  // Find all possible X positions
  const possibleX = [cellX - 1, cellX - 2, cellX + 1, cellX + 2].filter(function(cellPosition) {
    return (cellPosition > 0 && cellPosition < 9);
  });
  // Find all possible Y postions
  const possibleY = [cellY - 1, cellY - 2, cellY + 1, cellY + 2].filter(function(cellPosition) {
    return (cellPosition > 0 && cellPosition < 9);
  });

  for (let i = 0; i < possibleX.length; i++) {
    for (let j = 0; j < possibleY.length; j++) {

      if (Math.abs(cellX - possibleX[i]) + Math.abs(cellY - possibleY[j]) === 3 && cellY !== 0 && cellY < 9) {
        possiblePositions.push([xCoordinates[possibleX[i] -1], possibleY[j]]);
      }
    }
  }
  // Show valid positions on the page in the correct format
  possiblePositions.forEach(function(item) {
    const positions = item.join('');
    positionsToShow.push(positions);
    results.innerHTML = positionsToShow.join(' ');
  });

}); // end of click function
